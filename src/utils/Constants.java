package utils;

public class Constants {
	public static final String[] shapesList = { "Triangle", "Rectangle",
			"Square", "Parallelogram", "Circle" };
	
	public static final int textFieldSize = 5;
	public static final  int tableSize = 80;

}
