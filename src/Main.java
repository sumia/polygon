/**
 * runs app
 * 
 */
public class Main {
	public static void main(String args[]) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				PolygonView view = new PolygonView();
				ShapeFactory model = new ShapeFactory();
				
				new PolygonController(view, model);
			}
		});
	}
}
