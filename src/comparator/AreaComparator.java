package comparator;

import java.util.Comparator;

import shapes.Shape;

public class AreaComparator implements Comparator<Shape> {

	@Override
	public int compare(Shape shape1, Shape shape2) {
		return (int) (shape1.getArea() - shape2.getArea());
	}

}
