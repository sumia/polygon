
import shapes.Circle;
import shapes.Parallelogram;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;
import shapes.Triangle;

/**
 * The model for shapes
 */
public class ShapeFactory {

	public Shape getShape(String shapeType) {
		if (shapeType == null) {
			return null;
		}
		
		if (shapeType.equalsIgnoreCase("Triangle")) {
			return new Triangle();
		} else if (shapeType.equalsIgnoreCase("Rectangle")) {
			return new Rectangle();
		} else if (shapeType.equalsIgnoreCase("Square")) {
			return new Square();
		} else if (shapeType.equalsIgnoreCase("Parallelogram")) {
			return new Parallelogram();
		} else if (shapeType.equalsIgnoreCase("Circle")) {
			return new Circle();
		} 
		
		return null;
	}
}
