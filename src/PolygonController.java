import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JButton;
import javax.swing.JComboBox;

import comparator.AreaComparator;

import shapes.Circle;
import shapes.Parallelogram;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;
import shapes.Triangle;

/**
 * The Controller coordinates interactions between the View and Model
 */

public class PolygonController {
	private PolygonView view;
	private ShapeFactory model;

	private String selectedItem;
	private Shape shape;

	private ArrayList<Shape> list;

	/**
	 * constructor
	 * 
	 * @param view
	 * @param model
	 */
	public PolygonController(PolygonView view, ShapeFactory model) {
		this.view = view;
		this.model = model;

		list = new ArrayList<Shape>();
		setListeners();
	}

	/**
	 * sets various listener of gui components
	 */
	private void setListeners() {
		view.addComboBoxListener(new Listener());
		view.setComboBoxItemSelectedAtIndex(0);
		view.setComboBoxEditable(false);

		view.addBtnListener(new Listener());
	}

	/**
	 * 
	 * Listener class for Calculate button and ComboBox
	 * 
	 */
	private class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();

			if (obj instanceof JComboBox) {

				JComboBox comboBox = (JComboBox) e.getSource();
				selectedItem = (String) comboBox.getSelectedItem();

				shape = model.getShape(selectedItem);
				updateView(shape);

			} else if (obj instanceof JButton) {

				shape = model.getShape(selectedItem);
				calculateAndUpdateArea();

			}
		}
	}

	/**
	 * sets parameters names based on their shapes.
	 * 
	 * @param shape
	 */
	private void updateView(Shape shape) {

		view.setJPanelVisibility(view.getPanel1(), true);
		view.setJPanelVisibility(view.getPanel2(), true);

		view.setTextField1("");
		view.setTextField2("");

		view.setArea("");

		if (shape instanceof Triangle || shape instanceof Parallelogram) {

			view.setLabel1("Base:");
			view.setLabel2("Height:");

		} else if (shape instanceof Circle) {

			view.setLabel1("Radius");
			view.setJPanelVisibility(view.getPanel2(), false);

		} else if (shape instanceof Rectangle) {

			view.setLabel1("Width:");
			view.setLabel2("Height:");

		} else if (shape instanceof Square) {

			view.setLabel1("Length:");
			view.setJPanelVisibility(view.getPanel2(), false);

		}
	}

	/**
	 * calculates area and updates view
	 */
	private void calculateAndUpdateArea() {
		if (shape != null) {

			double area = 0;
			int num1 = view.getTextField1();
			int num2 = 0;

			if (shape instanceof Triangle) {
				num2 = view.getTextField2();
				
				Triangle triangle = (Triangle) shape;
				triangle.setBase(num1);
				triangle.setHeight(num2);
				area = triangle.calculateArea();

			} else if (shape instanceof Circle) {

				Circle circle = (Circle) shape;
				circle.setRadius(num1);
				area = circle.calculateArea();

			} else if (shape instanceof Parallelogram) {
				num2 = view.getTextField2();
				
				Parallelogram parallelogram = (Parallelogram) shape;
				parallelogram.setBase(num1);
				parallelogram.setHeight(num2);
				area = parallelogram.calculateArea();

			} else if (shape instanceof Rectangle) {

				num2 = view.getTextField2();
				
				Rectangle rectangle = (Rectangle) shape;
				rectangle.setWidth(num1);
				rectangle.setHeight(num2);

				area = rectangle.calculateArea();

			} else if (shape instanceof Square) {

				Square square = (Square) shape;
				square.setLength(num1);
				area = square.calculateArea();

			}

			view.setArea(String.valueOf(area));
			list.add(shape);

			Comparator<Shape> comparator = new AreaComparator();
			Collections.sort(list, comparator);

			view.setShapeHistory(list);
		}
	}
}
