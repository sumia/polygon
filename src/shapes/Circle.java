package shapes;

public class Circle extends Shape {

	private int radius;

	public Circle(){
		super();
	}
	
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public double calculateArea() {
		shapeWithParams = shapeType + " (" + radius + ")"; 
		
		area =  Math.PI * radius * radius;
		return area;
	}


}
