package shapes;

public class Parallelogram extends Shape {

	private int base;
	private int height;

	public Parallelogram() {
		super();
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getBase() {
		return base;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public double calculateArea() {

		shapeWithParams = shapeType + " (" + base + ", " + height + ")";
		area = base * height;
		return area;
	}

}
