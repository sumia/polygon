package shapes;

public class Triangle extends Shape {

	private int base;
	private int height;

	public Triangle() {
		super();
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public double calculateArea() {
		shapeWithParams = shapeType + " (" + base + ", " + height + ")";
		area = base * height / 2.0;
		return area;
	}

}
