package shapes;

public class Square extends Shape {

	private int length;

	public Square() {
		super();
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;

	}

	@Override
	public double calculateArea() {
		
		shapeWithParams = shapeType + " (" + length + ")"; 
		
		area = length * length;
		return area;
	}
}
