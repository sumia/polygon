package shapes;
public abstract class Shape {
	
	String shapeType;
	String shapeWithParams;
	double area;

	/**
	 * constructor
	 * initializes name of each shape from classname
	 */
	public Shape() {
		shapeType = getClass().getSimpleName().toString();
	}

	/**
	 * 
	 * @return shape name
	 */
	String getShape() {
		return shapeType;
	}
	
	/**
	 * 
	 * @return shape name with parameters in numerical value
	 */
	public String getShapeWithParams() {
		return shapeWithParams;
	}
	
	/**
	 * 
	 * @return calculated area based on shape's parameters
	 */
	public double getArea() {
		return area;
	}

	abstract double calculateArea();
}
