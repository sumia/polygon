package shapes;

public class Rectangle extends Shape {

	private int width;
	private int height;

	public Rectangle() {
		super();
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getWidth() {
		return width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public double calculateArea() {
		
		shapeWithParams = shapeType + " (" + width + ", " + height +  ")"; 
		
		area = width * height;
		return area;
	}

}
