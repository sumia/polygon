import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import shapes.Shape;

public class PolygonView {

	private JComboBox<String> comboBox;

	private JLabel labelSelectShape, label1, label2, labelArea, labelAreaValue,
			labelListOfShapes;
	private JTextField textField1, textField2;

	private JButton btnCalculate;

	private JPanel jPanel1, jPanel2, jPanelCalculatedArea, jPanelShape, jPanelBtn;

	private JTable table;
	private DefaultTableModel tableModel;
	private JScrollPane scrollPane;

	/**
	 * constructor
	 */
	public PolygonView() {

		initializeComponents();
		createUI();
	}

	/**
	 * Initializes all GUI components
	 */
	private void initializeComponents() {
		labelSelectShape = new JLabel("Select Shape: ");
		comboBox = new JComboBox<String>(utils.Constants.shapesList);
		comboBox.setSelectedIndex(0);

		jPanelShape = new JPanel();
		jPanelShape.add(labelSelectShape);
		jPanelShape.add(comboBox);

		label1 = new JLabel("label1");
		label2 = new JLabel("label2");

		textField1 = new JTextField("", utils.Constants.textFieldSize);
		textField1.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if (!((c >= '0') && (c <= '9') || (c == '.')
						|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
					Toolkit.getDefaultToolkit().beep();
					e.consume();
				}
			}
		});

		textField2 = new JTextField("", utils.Constants.textFieldSize);
		textField2.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if (!((c >= '0') && (c <= '9') || (c == '.')
						|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
					Toolkit.getDefaultToolkit().beep();
					e.consume();
				}
			}
		});

		
		btnCalculate = new JButton("Calculate");

		jPanelBtn = new JPanel();
		jPanelBtn.add(btnCalculate);
		
		labelArea = new JLabel("Area: ");
		labelAreaValue = new JLabel("");

		labelListOfShapes = new JLabel("List of Shapes");

		jPanel1 = new JPanel();
		jPanel1.add(label1);
		jPanel1.add(textField1);

		jPanel2 = new JPanel();
		jPanel2.add(label2);
		jPanel2.add(textField2);

		jPanelCalculatedArea = new JPanel();
		jPanelCalculatedArea.add(labelArea);
		jPanelCalculatedArea.add(labelAreaValue);

		tableModel = new DefaultTableModel(new Object[][] {},
				new Object[] { "" });
		table = new JTable(tableModel);
		table.setTableHeader(null);
		table.setShowGrid(true);

		scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(utils.Constants.tableSize,
				utils.Constants.tableSize));
		scrollPane.setBackground(Color.white);
	}

	/**
	 * creates user interface
	 */
	private void createUI() {

		JFrame frame = new JFrame("Polygon Area Calculation");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addComponentToPane(frame.getContentPane());

		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * adds components to the frame container
	 */
	private void addComponentToPane(Container container) {
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

		container.add(jPanelShape);
		container.add(jPanel1);
		container.add(jPanel2);
		container.add(jPanelBtn);
		container.add(jPanelCalculatedArea);
		container.add(labelListOfShapes);
		container.add(scrollPane);
		
		jPanelShape.setAlignmentX(Component.LEFT_ALIGNMENT);
		jPanel1.setAlignmentX(Component.LEFT_ALIGNMENT);
		jPanel2.setAlignmentX(Component.LEFT_ALIGNMENT);
		jPanelBtn.setAlignmentX(Component.LEFT_ALIGNMENT);
		jPanelCalculatedArea.setAlignmentX(Component.LEFT_ALIGNMENT);
		labelListOfShapes.setAlignmentX(Component.LEFT_ALIGNMENT);
		
	}

	/**
	 * sets first parameter of shape
	 * 
	 * @param name
	 *            first parameter e.g. base, width
	 */
	public void setLabel1(String name) {
		label1.setText(name);
	}

	/**
	 * returns value of first first parameter of each shape of shape available
	 * 
	 * @return value of first parameter
	 */
	public int getTextField1() {
		return Integer.parseInt(textField1.getText().toString());
	}

	/**
	 * sets first parameter value to the field
	 * 
	 * @param value
	 */
	public void setTextField1(String value) {
		textField1.setText(value);
	}

	/**
	 * sets second parameter of shape
	 * 
	 * @param name
	 *            first parameter e.g. height
	 */

	public void setLabel2(String name) {
		label2.setText(name);
	}

	/**
	 * returns value of first first parameter of each shape of shape available
	 * 
	 * @return value of second parameter
	 */
	public int getTextField2() {
		return Integer.parseInt(textField2.getText().toString());
	}

	/**
	 * sets second parameter value to the field
	 * 
	 * @param value
	 */
	public void setTextField2(String value) {
		textField2.setText(value);
	}

	/**
	 * sets area value to the label
	 * 
	 * @param area
	 *            of selected shape
	 */
	public void setArea(String area) {
		labelAreaValue.setText(area);
	}

	/**
	 * 
	 * @return parent of first parameter label and textfield
	 */
	public JPanel getPanel1() {
		return jPanel1;
	}

	/**
	 * 
	 * @return parent of second parameter label and textfield
	 */
	public JPanel getPanel2() {
		return jPanel2;
	}

	/**
	 * returns history of calculated shapes in ascending order of area
	 * 
	 * @return table
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * adds listener to the comboBox
	 * 
	 * @param listener
	 */
	public void addComboBoxListener(ActionListener listener) {
		comboBox.addActionListener(listener);
	}

	/**
	 * adds listener to the calculate button
	 * 
	 * @param listener
	 */
	public void addBtnListener(ActionListener listener) {
		btnCalculate.addActionListener(listener);
	}

	/**
	 * determines whether the JComboBox field is editable.
	 * 
	 * @param value
	 *            boolean value where true indicates that the field is editable
	 */
	public void setComboBoxEditable(boolean value) {
		comboBox.setEditable(value);
	}

	/**
	 * Selects the item at given index
	 * 
	 * @param index
	 *            an integer specifying the list item to select, where 0
	 *            specifies the first item in the list
	 */
	public void setComboBoxItemSelectedAtIndex(int index) {
		comboBox.setSelectedIndex(index);
	}

	/**
	 * Makes the specified panel visible or invisible.
	 * 
	 * some shapes have only 1 parameter e.g. circle has parameter radius only.
	 * so it is required to hide the 2nd parameter containing panel.
	 * 
	 * 
	 * @param jPanel
	 *            the panel
	 * @param value
	 *            boolean value where true indicates that the panel is visible
	 */
	public void setJPanelVisibility(JPanel jPanel, boolean value) {
		jPanel.setVisible(value);
	}

	/**
	 * adds calculated shapes to the table with their parameters in ascending
	 * order of their area
	 * 
	 * @param list
	 *            contains sorted shapes according to area
	 */
	public void setShapeHistory(ArrayList<Shape> list) {
		tableModel.setRowCount(0);
		for (Shape shape : list) {
			tableModel.addRow(new Object[] { shape.getShapeWithParams() });
		}
	}
}
